package com.github.alvarosct02.demojoinnus.features.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.github.alvarosct02.demojoinnus.R;
import com.github.alvarosct02.demojoinnus.data.PreferenceManager;
import com.github.alvarosct02.demojoinnus.data.models.User;
import com.github.alvarosct02.demojoinnus.features.map.MapsActivity;

public class LoginActivity extends AppCompatActivity {

    private CallbackManager callbackManager;

    public static Intent newInstance(Context context) {
        Intent i = new Intent(context, LoginActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");

        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                startActivity(MapsActivity.newInstance(LoginActivity.this));
                PreferenceManager.getInstance(LoginActivity.this).saveUser(new User());
                finish();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
