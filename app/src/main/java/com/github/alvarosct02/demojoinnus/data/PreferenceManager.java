package com.github.alvarosct02.demojoinnus.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.github.alvarosct02.demojoinnus.BuildConfig;
import com.github.alvarosct02.demojoinnus.data.models.User;
import com.github.alvarosct02.demojoinnus.utils.Constants;
import com.google.gson.Gson;


public class PreferenceManager {
    private static PreferenceManager INSTANCE = null;

    public static PreferenceManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new PreferenceManager(context);
        }
        return INSTANCE;
    }

    private final SharedPreferences prefs;

    private PreferenceManager(Context context) {
        prefs = context.getSharedPreferences(BuildConfig.PREF_FILE, Context.MODE_PRIVATE);
    }

    public boolean isUserLogged() {
        return getUser() != null;
    }

    public User getUser() {
        String userStr = prefs.getString(Constants.PREF_USER, "");
        return new Gson().fromJson(userStr, User.class);
    }

    public void saveUser(User user) {
        String userStr = new Gson().toJson(user, User.class);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.PREF_USER, userStr);
        editor.apply();
    }

    public void logout() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(Constants.PREF_USER);
        editor.apply();
    }

}
