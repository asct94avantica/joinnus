package com.github.alvarosct02.demojoinnus.features.splash;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.github.alvarosct02.demojoinnus.R;
import com.github.alvarosct02.demojoinnus.data.PreferenceManager;
import com.github.alvarosct02.demojoinnus.features.login.LoginActivity;
import com.github.alvarosct02.demojoinnus.features.map.MapsActivity;

public class SplashActivity extends AppCompatActivity {


    private static final int DELAY_MILLIS = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onSplashDone();
            }
        }, DELAY_MILLIS);
    }

    private void onSplashDone() {
        if (PreferenceManager.getInstance(this).isUserLogged()) {
            startActivity(MapsActivity.newInstance(this));
        } else {
            startActivity(LoginActivity.newInstance(this));
        }
        finish();
    }
}
